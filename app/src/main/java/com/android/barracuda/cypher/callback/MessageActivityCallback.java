package com.android.barracuda.cypher.callback;

import com.android.barracuda.model.Message;

public interface MessageActivityCallback {
  void processMessage(Message msg);
}
