package com.android.barracuda.cypher.models;

import java.math.BigInteger;

public class DHKeys {
  public int id;
  public BigInteger p;
  public BigInteger g;
  public BigInteger pubKey;
  public BigInteger prvKey;
  public long timestamp;
}
